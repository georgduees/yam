#Yet Another Matrix
#Buildlog

##Dependencies

##Libraries Used

## ToDo
WebFeatures:
- [X] HTML5 Canvas in Webinterface
- [X] Connection to Golang Server to display current Image
- [X] View Info from Server in Logs like MQTT Actions
- [ ] Send Info from Server to Browser
- [ ] Piskel File RestAPI
- [ ] Piskel View on Display
- [ ] View MQTT Events
- [ ] View MQTT Events from Topic
- [ ] Logic to Draw Piskel-Animation on MQTT-Event

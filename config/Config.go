package config 

import (
 "encoding/json"
	"io/ioutil"
	"net/http"
	"path/filepath"

	"time"

	"github.com/shibukawa/configdir"
)

//structs
type Config struct {
	Width      int    `json:"width"`
	Height     int    `json:"height"`
	SerpentineLayout   bool   `json:"serpentineLayout"`
	Port       int    `json:"port"`
	GIFPath    string `json:"gifpath"`
	Name       string `json:"name"`
	Init       bool
	Loglevel   int `json:"loglevel"`
	UpdateTime int `json:"updatetime"`
}

// DefaultConfig is the default configuration from the app
var DefaultConfig = Config{
	Width:      16,
	Height:     16,
	SerpentineLayout: false,
	Name:       "bpi",
	Port:       5000,
	Init:       true,
	UpdateTime: 0,
	Loglevel:   0,
	GIFPath:    "gif",
}

// ReadConfiguration reads the configuration from the file
func ReadConfiguration() Config {
	var config Config

	configDirs := configdir.New("GFD", "battlepi")
	// optional: local path has the highest priority
	configDirs.LocalPath, _ = filepath.Abs(".")
	folder := configDirs.QueryFolderContainsFile("setting.json")
	if folder != nil {
		data, _ := folder.ReadFile("setting.json")
		json.Unmarshal(data, &config)
	} else {
		config = DefaultConfig
	}
	return config
}

// SaveConfiguration saves the configuration
func SaveConfiguration(c Config) Config {
	configDirs := configdir.New("GFD", "battlepi")
	// optional: local path has the highest priority
	configDirs.LocalPath, _ = filepath.Abs(".")
	c.Init = false
	c.UpdateTime = int(time.Now().Unix())
	data, _ := json.Marshal(&c)
	// Stores to local folder
	folders := configDirs.QueryFolders(configdir.Local)
	folders[0].WriteFile("setting.json", data)
	return c
}
func GetConfig(w http.ResponseWriter, r *http.Request) {
	c := ReadConfiguration()
	json.NewEncoder(w).Encode(c)
}

func UpdateConfig(w http.ResponseWriter, r *http.Request) {

	var updateConfig Config

	reqBody, _ := ioutil.ReadAll(r.Body)
	
	json.Unmarshal(reqBody, &updateConfig)
	SaveConfiguration(updateConfig)
	c := ReadConfiguration()
	json.NewEncoder(w).Encode(c)
}
func ResetConfig(w http.ResponseWriter, r *http.Request) {

	SaveConfiguration(DefaultConfig)
	c := ReadConfiguration()
	json.NewEncoder(w).Encode(c)
}

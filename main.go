package main

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"image/color"
	"time"

	"github.com/mcuadros/go-rpi-ws281x"

	message "./web"

	cfg "./config"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/golang/protobuf/proto"
	mux "github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
)

//websocket stuff
var data []byte
var matrixData []uint32
var clients = make([]*websocket.Conn, 512)
var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin:     func(r *http.Request) bool { return true },
}
var brightness int
var (
	pin        = flag.Int("gpio-pin", 18, "GPIO pin")
	width      = flag.Int("width", 16, "LED matrix width")
	height     = flag.Int("height", 16, "LED matrix height")
	brightness = flag.Int("brightness", 32, "Brightness (0-255)")
)


type mqttStruct struct {
	Device      string `json:"device"`
	Brightness  int    `json:"brightness"`
	Humidity    int    `json:"humidity"`
	Temperature int    `json:"temperature"`
}

var wcStatus mqttStruct
var lastLogLine string

//piskelstructs

type Chunk struct {
	Layout    [][]int `json:"layout"`
	Base64PNG string  `json:"base64PNG"`
}
type Layer struct {
	Name       string  `json:"name"`
	Opacity    int     `json:"opacity"`
	FrameCount int     `json:"frameCount"`
	Chunks     []Chunk `json:"chunks"`
}

type Piskel struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	FPS         int    `json:"fps"`
	Height      int    `json:"height"`
	Width       int    `json:"width"`
	Layers      []Layer
	LayerString []string `json:"layers"`
}

type PiskelFile struct {
	ModelVersion int    `json:"modelVersion"`
	Piskel       Piskel `json:"piskel"`
}

//mqtt
var f MQTT.MessageHandler = func(client MQTT.Client, msg MQTT.Message) {
	err := json.Unmarshal(msg.Payload(), &wcStatus)
	if err != nil {
		log.Error(err)
	}
	pmsg := &message.Message{
		Id:         int32(0),
		Data:       matrixData,
		Brightness: int32(wcStatus.Brightness),
		Png:        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAErElEQVR4nO3aIa4VWRSFYSbRQYBr7FOoNliG0IYxoJkJs8AiENgeALox3dMAbOUlvJPKqZN1du3vT7avund9rp791JO9evGy9OnpnqUfYPfSAwZgbQAMSg8YgLUBMCg9YADWBsCg9IABWBsAg9IDBmBtAAxKDxiAtQEwKD1gANYGwKD0gAFYGwCD0gMGYG0ADEoPGIC1ATAoPWAA1tYOQHqQd7vqAeAA6FR6MHe76gHgAOhUejB3u+oB4ADoVHowd7vqAeAA6FR6MHe76gHgAOhUejB3u+oB4AAo3X//uAuvGxgAHACl22A0dzoAqrXBaO50AFRrg9Hc6QCo1gajudMBUK0NRnOnA6BaG4zmTgdAtTYYzZ0OgGptMJo7HQDV2mA0dzoAqrXBaO50AIT74/mfW93qwaXfb/Z9Abi49AAAACBaegAAABAtPQAAAIiWHgAAAERLDwAAAKKlBwAAANHSAwAAgGjpAQAAQLT0AAAAIFp6AAAAEC09AAAAiHb2D/v0/7dTlx7Y7O32vgBcXPVBAADAVNUHAQAAU1UfBAAATFV9EAAAMFX1QQAAwFTVBwEAAFNVHwQAAExVfRAAADBV9UEAAMBU1QcBAABTVR8EAABMlR7Y4/vx8d3SS7/f4/MxXLj0AAAAIFp6AAAAEC09AAAAiJYeAAAAREsPAAAAoqUHAAAA0dIDAACAaOkBAABAtPQAAAAgWnoAAAAQLT0AAACo1ck/7OytBrD6+Zdf8QAAAIDSAQDARAAAAEDpAABgIgAAAKB0AAAwEQAAAFA6AACYCAAAACgdAABMBAAAALQqPZjdr1kAOABalR7Y7tcsABwArUoPbPdrFgAOgFalB7b7NQsAB0Cr0gPb/ZoFgAOgVemB7X7NAsABoN/38eu/p+7zm4eld/Z5drvdAmAQAAC0DgAAWgcAAK0DAIDWAQBA6wAAoHUAANA6AABoHQAAtA4AAFoHAAD36uTHYd0A/Pj+5dSt/j1XBwAAALQKAAAOAQAAAK0CAIBDAAAAQKsAAOAQAAAA0CoAADgEAAAAtAoAAA4BAAAArQIAgEMAAABAqwAA4BAAAADQKgAAOAQAAAC0CgAADgEAAACtAgCAQwAAAECrAADgEAAAAKDf9/rh4dT99e7D0jv7PKuvegAMAgCA1gEAQOsAAKB1AADQOgAAaB0AALQOAABaBwAArQMAgNYBAEDrAACgde//fuueuOoBMCg9sN2vegAMSg9s96seAIPSA9v9qgfAoPTAdr/qATAoPbDdr3oADEoPbPerHgCD0gPb/aoHwKD0wHa/6gEwKD2w3a96AAxKD2z3qx4Ag9ID2/2qB8Cg9MAe36sXL5detwAYlB48AGsDYFB68ACsDYBB6cEDsDYABqUHD8DaABiUHjwAawNgUHrwAKwNgEHpwQOwNgAGpQcPwNoAGJQePABrA2BQevAArA2AQenBA7A2AC7OQGsFwMUBUCsALg6AWgFwcQDUCoCLA6BWAFwcALUC4OIAqBUAFwdArQC4OABqBcDFAVArAC4OgFoBcHEA1AoAtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4Ate4XZRHIFDU7GGkAAAAASUVORK5CYII=",
		Severity:   "info",
		Message:    "random test",
	}
	data, _ = proto.Marshal(pmsg)
	//log.Info(&wcStatus.Brightness)
}

//protobufstuff

func handler(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}
	clients = append(clients, conn)
	for {
		_, _, err := conn.ReadMessage()
		if err != nil {
			return
		}

		if err = conn.WriteMessage(websocket.BinaryMessage, data); err != nil {
			return
		}
	}
}

//fileupload
const maxUploadSize = 10 * 1024 * 1024 // 2 MB
const uploadPath = "./web/uploads"

func randToken(len int) string {
	b := make([]byte, len)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}
func renderError(w http.ResponseWriter, message string, statusCode int) {
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(message))
}

func uploadFileHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// validate file size
		r.Body = http.MaxBytesReader(w, r.Body, maxUploadSize)
		if err := r.ParseMultipartForm(maxUploadSize); err != nil {
			renderError(w, "FILE_TOO_BIG", http.StatusBadRequest)
			return
		}
		var Buf bytes.Buffer
		// in your case file would be fileupload
		file, header, err := r.FormFile("uploadfile")
		if err != nil {
			panic(err)
		}
		defer file.Close()
		name := strings.Split(header.Filename, ".")

		newPath := filepath.Join(uploadPath, name[0]+"."+name[1])

		// write file
		newFile, err := os.Create(newPath)
		io.Copy(&Buf, file)
		// do something with the contents...
		// I normally have a struct defined and unmarshal into a struct, but this will
		// work as an example
		contents := Buf.String()
		log.Info(contents)
		// I reset the buffer in case I want to use it again
		// reduces memory allocations in more intense projects
		Buf.Reset()

		if err != nil {
			renderError(w, "CANT_WRITE_FILE", http.StatusInternalServerError)
			return
		}
		defer newFile.Close() // idempotent, okay to call twice
		io.Copy(newFile, file)
		http.Redirect(w, r, "http://box.local:5000/", 301)
	})
}

func OSReadDir(root string) ([]string, error) {
	var files []string
	f, err := os.Open(root)
	if err != nil {
		return files, err
	}
	fileInfo, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return files, err
	}
	for _, file := range fileInfo {
		if strings.HasSuffix(file.Name(), ".piskel") {
			files = append(files, file.Name())
		}
	}
	return files, nil
}
func loadPiskel(filePath string) *PiskelFile {
	var p *PiskelFile
	log.Info(filePath)
	contents, _ := ioutil.ReadFile(filePath)
	fmt.Println(string(contents))
	//b, _ := ioutil.ReadFile(filePath)
	e := json.Unmarshal([]byte(contents), &p)
	if e != nil {
		log.Error(e)
	}
	for _, value := range p.Piskel.LayerString {
		fmt.Println(value)
		var l Layer
		lErr := json.Unmarshal([]byte(value), &l)
		if lErr != nil {
			fmt.Println(lErr)
		}
		p.Piskel.Layers = append(p.Piskel.Layers, l)

	}
	return p
}
type FrameSet struct{
	WaitTime int,
	
}
func getFrameSetFromPiskel(pFile PiskelFile)FrameSet{
	var fSet FrameSet

	return fSet
}
func main() {
	customFormatter := new(logrus.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	logrus.SetFormatter(customFormatter)
	log.Info("Starting wigo runth Reading Configuration")
	//ReadConfiguration
	x := cfg.ReadConfiguration()
	log.Info("Printing Configuration")
	log.Info(x)
	log.Info("Loglevel:", x.Loglevel)
	log.Info("Loading Plugins")
	x = cfg.SaveConfiguration(x)
	//check mqtt
	opts := MQTT.NewClientOptions().AddBroker("tcp://192.168.0.20:1883")
	opts.SetClientID("go-simple")
	opts.SetDefaultPublishHandler(f)

	//create and start a client using the above ClientOptions
	c := MQTT.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	//subscribe to the topic /go-mqtt/sample and request messages to be delivered
	//at a maximum qos of zero, wait for the receipt to confirm the subscription
	if token := c.Subscribe("wcTopic", 0, nil); token.Wait() && token.Error() != nil {
		fmt.Println(token.Error())

	}
	config := ws281x.DefaultConfig
	config.Brightness = *brightness
	config.Pin = *pin

	c, err := ws281x.NewCanvas(*width, *height, &config)
	fatal(err)

	defer c.Close()
	err = c.Initialize()
	fatal(err)

	bounds := c.Bounds()

	

	//load plugins
	//check enabled plugins
	//display plugin setup
	//Read PiskeFile
	//sendToWeb:
	cData := [][]uint32{
		{
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xffaaccff, 0xffaaccff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xffaaccff, 0xffaaccff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffaaccff, 0xffaaccff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000,
			0xffaaccff, 0xffaaccff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000,
			0xff53257e, 0xff53257e, 0xff000000, 0xff000000, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff53257e, 0xff53257e, 0xffe8f1ff, 0xffe8f1ff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000,
			0xff53257e, 0xff53257e, 0xff000000, 0xff000000, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff53257e, 0xff53257e, 0xffe8f1ff, 0xffe8f1ff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff4f575f, 0xff4f575f, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff4f575f, 0xff4f575f, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff4f575f, 0xff4f575f, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff4f575f, 0xff4f575f, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		},
		{
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xffffad29, 0xffffad29, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xffffad29, 0xffffad29, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xffaaccff, 0xffaaccff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xffaaccff, 0xffaaccff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xffffad29, 0xffffad29, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xffffad29, 0xffffad29, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffad29, 0xffffad29, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xffffad29, 0xffffad29, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xffe8f1ff, 0xffe8f1ff, 0xffaaccff, 0xffaaccff, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xffe8f1ff, 0xffe8f1ff, 0xffaaccff, 0xffaaccff, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff53257e, 0xff53257e, 0xffe8f1ff, 0xffe8f1ff, 0xff53257e, 0xff53257e, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff53257e, 0xff53257e, 0xffe8f1ff, 0xffe8f1ff, 0xff53257e, 0xff53257e, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		},
		{
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xffaaccff, 0xffaaccff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xffaaccff, 0xffaaccff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffaaccff, 0xffaaccff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000,
			0xffaaccff, 0xffaaccff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000,
			0xff53257e, 0xff53257e, 0xff000000, 0xff000000, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff53257e, 0xff53257e, 0xffe8f1ff, 0xffe8f1ff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000,
			0xff53257e, 0xff53257e, 0xff000000, 0xff000000, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff53257e, 0xff53257e, 0xffe8f1ff, 0xffe8f1ff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff4f575f, 0xff4f575f, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff4f575f, 0xff4f575f, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff4f575f, 0xff4f575f, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff4f575f, 0xff4f575f, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
		},
		{
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xffe8f1ff, 0xffe8f1ff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xffaaccff, 0xffaaccff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xffaaccff, 0xffaaccff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff27ffff, 0xff27ffff, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffffad29, 0xffffad29, 0xff000000, 0xff000000, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000, 0xff000000, 0xff000000,
			0xffaaccff, 0xffaaccff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000,
			0xffaaccff, 0xffaaccff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffc7c3c2, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff000000, 0xff000000,
			0xff53257e, 0xff53257e, 0xff000000, 0xff000000, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff53257e, 0xff53257e, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000,
			0xff53257e, 0xff53257e, 0xff000000, 0xff000000, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xffe8f1ff, 0xff53257e, 0xff53257e, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xffaaccff, 0xff000000, 0xff000000,
		},
	}
	matrixData = cData[0]
	msg := &message.Message{
		Id:         int32(0),
		Data:       cData[0],
		Brightness: 100,
		Png:        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAErElEQVR4nO3aIa4VWRSFYSbRQYBr7FOoNliG0IYxoJkJs8AiENgeALox3dMAbOUlvJPKqZN1du3vT7avund9rp791JO9evGy9OnpnqUfYPfSAwZgbQAMSg8YgLUBMCg9YADWBsCg9IABWBsAg9IDBmBtAAxKDxiAtQEwKD1gANYGwKD0gAFYGwCD0gMGYG0ADEoPGIC1ATAoPWAA1tYOQHqQd7vqAeAA6FR6MHe76gHgAOhUejB3u+oB4ADoVHowd7vqAeAA6FR6MHe76gHgAOhUejB3u+oB4AAo3X//uAuvGxgAHACl22A0dzoAqrXBaO50AFRrg9Hc6QCo1gajudMBUK0NRnOnA6BaG4zmTgdAtTYYzZ0OgGptMJo7HQDV2mA0dzoAqrXBaO50AIT74/mfW93qwaXfb/Z9Abi49AAAACBaegAAABAtPQAAAIiWHgAAAERLDwAAAKKlBwAAANHSAwAAgGjpAQAAQLT0AAAAIFp6AAAAEC09AAAAiHb2D/v0/7dTlx7Y7O32vgBcXPVBAADAVNUHAQAAU1UfBAAATFV9EAAAMFX1QQAAwFTVBwEAAFNVHwQAAExVfRAAADBV9UEAAMBU1QcBAABTVR8EAABMlR7Y4/vx8d3SS7/f4/MxXLj0AAAAIFp6AAAAEC09AAAAiJYeAAAAREsPAAAAoqUHAAAA0dIDAACAaOkBAABAtPQAAAAgWnoAAAAQLT0AAACo1ck/7OytBrD6+Zdf8QAAAIDSAQDARAAAAEDpAABgIgAAAKB0AAAwEQAAAFA6AACYCAAAACgdAABMBAAAALQqPZjdr1kAOABalR7Y7tcsABwArUoPbPdrFgAOgFalB7b7NQsAB0Cr0gPb/ZoFgAOgVemB7X7NAsABoN/38eu/p+7zm4eld/Z5drvdAmAQAAC0DgAAWgcAAK0DAIDWAQBA6wAAoHUAANA6AABoHQAAtA4AAFoHAAD36uTHYd0A/Pj+5dSt/j1XBwAAALQKAAAOAQAAAK0CAIBDAAAAQKsAAOAQAAAA0CoAADgEAAAAtAoAAA4BAAAArQIAgEMAAABAqwAA4BAAAADQKgAAOAQAAAC0CgAADgEAAACtAgCAQwAAAECrAADgEAAAAKDf9/rh4dT99e7D0jv7PKuvegAMAgCA1gEAQOsAAKB1AADQOgAAaB0AALQOAABaBwAArQMAgNYBAEDrAACgde//fuueuOoBMCg9sN2vegAMSg9s96seAIPSA9v9qgfAoPTAdr/qATAoPbDdr3oADEoPbPerHgCD0gPb/aoHwKD0wHa/6gEwKD2w3a96AAxKD2z3qx4Ag9ID2/2qB8Cg9MAe36sXL5detwAYlB48AGsDYFB68ACsDYBB6cEDsDYABqUHD8DaABiUHjwAawNgUHrwAKwNgEHpwQOwNgAGpQcPwNoAGJQePABrA2BQevAArA2AQenBA7A2AC7OQGsFwMUBUCsALg6AWgFwcQDUCoCLA6BWAFwcALUC4OIAqBUAFwdArQC4OABqBcDFAVArAC4OgFoBcHEA1AoAtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4Ate4XZRHIFDU7GGkAAAAASUVORK5CYII=",
		Severity:   "info",
		Message:    "Testnachricht",
	}
	data, _ = proto.Marshal(msg)
	//loadPiskels
	fList, _ := OSReadDir("./web/uploads")
	var wcList []string
	var regList []string
	for _, filename := range fList {

		if strings.HasPrefix(filename, "wc") {
			wcList = append(wcList, filename)
		} else {
			regList = append(regList, filename)
		}
	}
	log.Info("wc animations")

	log.Info(len(wcList))

	log.Info("regular animations")
	log.Info(len(regList))

	pFile := loadPiskel("./web/uploads/" + wcList[0])
	fmt.Println(pFile.Piskel.Name)

	for{
		if(brightness>50){
			color := color.RGBA{255, 0, 0, 255}

			for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
				for x := bounds.Min.X; x < bounds.Max.X; x++ {
					c.Set(x, y, color)
					c.Render()
					time.Sleep(10 * time.Millisecond)
				}
			}
		}

		else{
			color := color.RGBA{0, 255, 0, 255}
			for y := bounds.Min.Y; y < bounds.Max.Y; y++ {
				for x := bounds.Min.X; x < bounds.Max.X; x++ {
					c.Set(x, y, color)
					c.Render()
					time.Sleep(10 * time.Millisecond)
				}
			}
		}
	}
	/*
		for index, cDat := range cData {

			msg := &message.Message{
				Id:         int32(index),
				Data:       cDat,
				Brightness: 100,
				Png:        "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMAAAADACAYAAABS3GwHAAAErElEQVR4nO3aIa4VWRSFYSbRQYBr7FOoNliG0IYxoJkJs8AiENgeALox3dMAbOUlvJPKqZN1du3vT7avund9rp791JO9evGy9OnpnqUfYPfSAwZgbQAMSg8YgLUBMCg9YADWBsCg9IABWBsAg9IDBmBtAAxKDxiAtQEwKD1gANYGwKD0gAFYGwCD0gMGYG0ADEoPGIC1ATAoPWAA1tYOQHqQd7vqAeAA6FR6MHe76gHgAOhUejB3u+oB4ADoVHowd7vqAeAA6FR6MHe76gHgAOhUejB3u+oB4AAo3X//uAuvGxgAHACl22A0dzoAqrXBaO50AFRrg9Hc6QCo1gajudMBUK0NRnOnA6BaG4zmTgdAtTYYzZ0OgGptMJo7HQDV2mA0dzoAqrXBaO50AIT74/mfW93qwaXfb/Z9Abi49AAAACBaegAAABAtPQAAAIiWHgAAAERLDwAAAKKlBwAAANHSAwAAgGjpAQAAQLT0AAAAIFp6AAAAEC09AAAAiHb2D/v0/7dTlx7Y7O32vgBcXPVBAADAVNUHAQAAU1UfBAAATFV9EAAAMFX1QQAAwFTVBwEAAFNVHwQAAExVfRAAADBV9UEAAMBU1QcBAABTVR8EAABMlR7Y4/vx8d3SS7/f4/MxXLj0AAAAIFp6AAAAEC09AAAAiJYeAAAAREsPAAAAoqUHAAAA0dIDAACAaOkBAABAtPQAAAAgWnoAAAAQLT0AAACo1ck/7OytBrD6+Zdf8QAAAIDSAQDARAAAAEDpAABgIgAAAKB0AAAwEQAAAFA6AACYCAAAACgdAABMBAAAALQqPZjdr1kAOABalR7Y7tcsABwArUoPbPdrFgAOgFalB7b7NQsAB0Cr0gPb/ZoFgAOgVemB7X7NAsABoN/38eu/p+7zm4eld/Z5drvdAmAQAAC0DgAAWgcAAK0DAIDWAQBA6wAAoHUAANA6AABoHQAAtA4AAFoHAAD36uTHYd0A/Pj+5dSt/j1XBwAAALQKAAAOAQAAAK0CAIBDAAAAQKsAAOAQAAAA0CoAADgEAAAAtAoAAA4BAAAArQIAgEMAAABAqwAA4BAAAADQKgAAOAQAAAC0CgAADgEAAACtAgCAQwAAAECrAADgEAAAAKDf9/rh4dT99e7D0jv7PKuvegAMAgCA1gEAQOsAAKB1AADQOgAAaB0AALQOAABaBwAArQMAgNYBAEDrAACgde//fuueuOoBMCg9sN2vegAMSg9s96seAIPSA9v9qgfAoPTAdr/qATAoPbDdr3oADEoPbPerHgCD0gPb/aoHwKD0wHa/6gEwKD2w3a96AAxKD2z3qx4Ag9ID2/2qB8Cg9MAe36sXL5detwAYlB48AGsDYFB68ACsDYBB6cEDsDYABqUHD8DaABiUHjwAawNgUHrwAKwNgEHpwQOwNgAGpQcPwNoAGJQePABrA2BQevAArA2AQenBA7A2AC7OQGsFwMUBUCsALg6AWgFwcQDUCoCLA6BWAFwcALUC4OIAqBUAFwdArQC4OABqBcDFAVArAC4OgFoBcHEA1AoAtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4AtQ4Ate4XZRHIFDU7GGkAAAAASUVORK5CYII=",
				Severity:   "info",
				Message:    "Testnachricht",
			}


		}
	*/
	//Startup Webserver
	r := mux.NewRouter()
	r.PathPrefix("/ws").HandlerFunc(handler)
	r.PathPrefix("/upload").HandlerFunc(uploadFileHandler())
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./web/")))
	host, _ := os.Hostname()
	adr := host + ":" + strconv.Itoa(x.Port)
	log.Info("Serving Webpage: ", adr)

	http.ListenAndServe(adr, r)

}

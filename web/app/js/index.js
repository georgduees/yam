var BattlePi = (function () {
  /* … private Objekte … */
  var cEl=null;
  var cWidth=640;
  var cHeight=640;
  var x=16;
  var y=16;
  var canvasArray=[];
  var imgPNG="";
  var pWidth=Math.floor(cWidth/x);
  var pHeight=Math.floor(cHeight/y);

  //Canvas Magic
  //change uint32 from message to rgba color
  function toColor(num) {
  num >>>= 0;
  var r = num & 0xFF,
  g = (num & 0xFF00) >>> 8,
  b = (num & 0xFF0000) >>> 16,
  a = ( (num & 0xFF000000) >>> 24 ) / 255 ;
  return "rgba(" + [r, g, b, a].join(",") + ")";
  }
  function drawGrid(el){
  var ctx=el.getContext('2d');
  ctx.beginPath();
  for (let i = 0; i < x; i++) {
    for (let j = 0; j < y; j++) {
    ctx.rect(i*pWidth,j*pHeight, pWidth,pHeight)
    }
  }
  ctx.stroke();
  return el;
  }
  function drawCanvas(el,data){
    var ctx=el.getContext('2d');
    ctx.beginPath();
    ctx.fillStyle ="black";
    k=0;
    for (let i = 0; i < x; i++) {
      for (let j = 0; j < y; j++) {
        ctx.fillStyle = toColor(data[k]);
        ctx.fillRect(i*pWidth,j*pHeight, pWidth,pHeight);
        k++;
      }
    }
    ctx.stroke();
    return el; 
  }
  function onMessage(event){
  var Message = builder.build("Message");
  var message = Message.decode(event.data);
  canvasArray=message.data;
  //      appendToLog({"Id":1337,"Severity":"info","message":"this is a testmesssage"});
  appendToLog(message);
  this.Draw();
  }
  function appendToLog(inputData){

  if ('content' in document.createElement('template')) {
  var fragment = document.getElementById('logItem');
  var lEl=document.importNode(fragment.content,true);
  lEl.querySelector(".tag").innerHTML=inputData.severity.toUpperCase();
  lEl.querySelector(".tag").classList.remove("is-info");
  lEl.querySelector(".tag").classList=lEl.querySelector(".tag").classList+" is-"+inputData.severity;

  var cDate = new Date();
  lEl.querySelector("#logDate").innerHTML=cDate.toTimeString();
  lEl.querySelector("#logText").innerHTML=inputData.message;
  document.getElementById("logBox").insertBefore(lEl,document.getElementById("logBox").firstChild);
  }

  }
  /* Gebe öffentliche API zurück: */
  return {
    Init:function () {
      cEl=document.getElementById('canvas');
      cWidth=cEl.width;
      cHeight=cEl.height;
      pWidth=Math.floor(cWidth/x);
      pHeight=Math.floor(cHeight/y);
      cEl=drawGrid(cEl);
    },
    Connect:function () {
      console.log("Connect");

      var ProtoBuf = dcodeIO.ProtoBuf;
      var builder = ProtoBuf.loadProtoFile("app/message.proto");
      //5000
      var loc = window.location.host;
      var conn = new WebSocket("ws://"+loc+"/ws");
      document.getElementById("uploadForm").setAttribute("action","http://"+loc+"/upload")
      conn.onopen = function () {
        console.log("Opening a connection...");
        conn.send('MSG');
        conn.binaryType = "arraybuffer";
      }
      conn.onclose = function(evt) {
      }
      conn.onmessage = function(event) {
        var Message = builder.build("Message");
        var message = Message.decode(event.data);
        canvasArray=message.data;
        imgPNG=message.png;
        appendToLog(message);
    }},
    Upload:function () {
      console.log("Upload");
    },
    Draw:function () {
      console.log("Draw");
      cEl=document.getElementById('canvas');
      drawCanvas(cEl,canvasArray);
      iEl=document.getElementById('image');
      iEl.src=imgPNG;
    }
  };

})();
